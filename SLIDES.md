---
marp: true
theme: gaia
_class: lead
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---

# Midi tech

Initiation à la modélisation 3D

![bg left:40% 80%](./assets/logo.svg)

---

# CAD / CAO ?

Computer-Aided Design ou Conception Assistée par Ordinateur.

Logiciels permettant de réaliser des modélisations géométriques d'objets, généralement utilisés pour modéliser des objets en 3D.

Parmi les plus connus : Autodesk Fusion 360, Solidworks, Freecad


---

# Freecad

Logiciel de CAD Open Source

Grande communauté avec beaucoup de tutos sur youtube.

![bg right:50% 80%](./assets/Freecad16.svg)



---

# Modélisation

**Comment ?**

A partir de datasheets

**Pour quels usages ?**

Impression 3D, Robotique etc...

![bg right:50% 80%](./assets/pion.jpg)

---

# Mise en pratique

Mise en pratique sous FreeCad.

---

# OpenSCAD

Alternative de modélisation à partir de code.

Extension VSCode disponible avec documentation, formatage et linter.

![bg right:60% 90%](./assets/openscad.jpeg)

---



<br >
<br >
<br >
<br >
<p style="text-align:center; vertical-align:middle;">
<strong>Merci pour votre participation 🙏</strong>
</p>